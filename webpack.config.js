/* global __dirname, module, require */
const path = require( 'path' );
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );

module.exports = {
	mode: 'production',
	entry: './src/main.ts',
	output: {
		filename: 'bundle.js',
		path: path.resolve( __dirname, 'dist' )
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					'babel-loader',
					'eslint-loader'
				]
			},
			{
				test: /\.ts$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					'ts-loader'
				]
			},
			{
				test: /\.s?css$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: '../'
						}
					},
					'css-loader',
					{
						loader: 'postcss-loader',
						options: {
							plugins: function() {
								return [
									require( 'autoprefixer' )
								];
							}
						}
					},
					'sass-loader'
				]
			},
			{
				test: /\.ttf$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					{
						loader: 'file-loader',
						options: {
							outputPath: './assets/fonts'
						}
					}
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin( {
			template: './src/index.html'
		} ),
		new MiniCssExtractPlugin( {
			filename: './assets/[name].css'
		} )
	]
};
