import './assets/style.scss';

/* MediaWiki API Docs:
 * https://www.mediawiki.org/wiki/API:Main_page
 */
async function wikipediaAPIRequest( queryStringParameters: string ) {
	if ( queryStringParameters !== '' ) {
		const wikipediaAPIResponse = fetch(
			`https://en.wikipedia.org/w/api.php?origin=*&format=json&formatversion=2&${queryStringParameters}`,
			{
				method: 'GET',
				headers: new Headers( {
					'Api-User-Agent': 'ZebsSomewhatPointlessAppForSearchingWikipedia/1.0 (https://zebulan.com; zebulanstanphill@protonmail.com)'
				} )
			}
		);
		return ( await wikipediaAPIResponse ).json();
	} else {
		throw new Error( 'queryStringParameters was an empty string' );
	}
}

async function getWikipediaSearchData( query: string ) {
	if ( query !== '' ) {
		const wikipediaSearchData =
			await wikipediaAPIRequest( `action=query&list=search&srsearch=${query}` );
		const wikipediaSearchDataQuery = ( await wikipediaSearchData ).query;

		return [ wikipediaSearchDataQuery.search, wikipediaSearchDataQuery.searchinfo ];
	} else {
		throw new Error( 'query was an empty string' );
	}
}

function wikipediaLinkFromPageID( pageID: number ) {
	if ( pageID !== undefined ) {
		return `https://en.wikipedia.org/?curid=${pageID}`;
	} else {
		throw new Error( `pageID is ${pageID} when it should be a non-empty string` );
	}
}

interface wikipediaSearchResult {
	pageid: number,
	snippet: string,
	title: string
}

const wikipediaSearchResultCard = ( searchResult: wikipediaSearchResult ) =>
	`<li>
		<a href="${wikipediaLinkFromPageID( searchResult.pageid )}">
			<article class="zspafsw-search-result">
				<h1>${searchResult.title}</h1>
				<p>${searchResult.snippet}…</p>
			</article>
		</a>
	</li>`;

async function zspafswUpdateDisplayedSearchResults() {
	const searchInputEl = document.getElementById( 'zspafsw-search-input' );
	if ( searchInputEl instanceof HTMLInputElement ) {
		const query = searchInputEl.value;

		const resultsListElement = document.getElementById( 'zspafsw-search-results-list' );

		// Empty element (to remove previous search results).
		resultsListElement.textContent = '';

		if ( query === '' ) {
			document.getElementById( 'zspafsw-search-results' ).classList.add( 'zeb-no-display' );
		} else {
			const resultsContainerElement = document.getElementById( 'zspafsw-search-results' );

			resultsContainerElement.classList.remove( 'zeb-no-display' );

			const [ searchResults ] = await getWikipediaSearchData( query );

			for ( const result of searchResults ) {
				resultsListElement.insertAdjacentHTML(
					'beforeend',
					wikipediaSearchResultCard( result )
				);
			}
		}
	} else {
		throw new Error( 'searchInputEl should be an HTMLInputElement' );
	}
}

( () => {
	document.getElementById( 'zspafsw-search-button' ).addEventListener(
		'click',
		zspafswUpdateDisplayedSearchResults
	);

	document.getElementById( 'zspafsw-search-input' ).addEventListener(
		'keydown',
		event => {
			if ( event.key === 'Enter' ) {

				/* The default behavior of an input field causes the value to
				 * be submitted and cleared, so it is disabled here since we
				 * are handling the input with client-side code.
				 */
				event.preventDefault();
				zspafswUpdateDisplayedSearchResults();
			}
		}
	);
} )(); 
